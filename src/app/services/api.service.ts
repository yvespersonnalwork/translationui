import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { HttpClient, Headers, RequestOptions } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  sendPostRequest(url: any, body: any) {
    var http = this.http
    return  new Promise(function(resolve, reject) {
      /* 
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('access-control-allow-origin', '*');
    headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,DELETE,PATCH');
    */
  
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Headers': 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range',
        'access-control-allow-origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,PUT,DELETE,PATCH'
      })
    };
 
    http.post(url,
    body,
    httpOptions
    )
    .subscribe(
        (val) => {
            console.log("POST call successful value returned in body", val);                 
            resolve(val)
        },
        response => {
            console.log("POST call in error", JSON.stringify(response));
            reject(response)
        },
        () => {
            console.log("The POST observable is now completed.");
        });
        
    });
    
  }
}
