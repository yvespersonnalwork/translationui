import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { TwoKeyMap } from '../services/TwoKeyMap';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage implements OnInit{
  translateRoute = new TwoKeyMap<any>();
  searchRoute = new TwoKeyMap<any>();
  reportRoute = new TwoKeyMap<any>();

  from = {
    "level":"word", 
    "sentence":"", 
    "from_lang": "french",
    "target_lang":"lingala"
  }

  translationResult = {
    level: "word", 
    sentence: "", 
    target_lang: "", 
    translated: ""
  }

  searchResult = {
    level: "word", 
    sentence: "", 
    target_lang: "", 
    samples: null
  }
  url = "http://3.15.209.27:5000";

  constructor(private apiService:ApiService, private sanitizer: DomSanitizer, public alertController: AlertController, public toastController: ToastController) {

    this.translateRoute = new TwoKeyMap<any>();
    this.translateRoute.set('french', 'lingala', {value: '/translate_fra'});
    this.translateRoute.set('french', 'swahili', {value: '/translate_fra'});

    this.searchRoute = new TwoKeyMap<any>();
    this.searchRoute.set('french', 'lingala', {value: '/search_fra'});
    this.searchRoute.set('french', 'swahili', {value: '/search_fra'});

    this.reportRoute = new TwoKeyMap<any>();
    this.reportRoute.set('french', 'lingala', {value: '/error_fra'});
    this.reportRoute.set('french', 'swahili', {value: '/error_fra'});

  }

  ngOnInit() { }
  translate(){  
  var controller = this;  
    var translationResult = this.translationResult;
    var searchResult = this.searchResult;

    var translateRoute = this.translateRoute;
    var searchRoute = this.searchRoute;
    var sanitizer = this.sanitizer;
    var url = this.url;
    var from = this.from;
      

      controller.apiService.sendPostRequest(url + searchRoute.get('french','lingala').value, from).then(function(value: any){
        
        searchResult.samples = sanitizer.bypassSecurityTrustHtml(value.samples);

        if(controller.isSentenceTranslate(value.sentence, value.samples) == 1){
          controller.apiService.sendPostRequest(url + translateRoute.get('french','lingala').value, from).then(function(value: any){
            translationResult.translated = value.translated;
          })
        }else{
          translationResult.translated = "N/A"
        }
 
      })
  }

  getWordsFromSentences(str: any){
    return str.split(" ");
  }

  isSentenceTranslate(str: any, comp: any){
    var response = 0;
    var text = this.getWordsFromSentences(str);
            console.log(text)

    var st;
    text.forEach(function(element) {

      if(comp.toString().includes(element + "</span>") > 0){
        response = 1;
      }
      
    });
   

    return response
  }

  languageFrom(str: any){
    console.log(this.from.target_lang);
    this.clear();
  }
  languageTo(str: any){
    this.clear();
  }

  clear(){
    this.translationResult.translated= ""
    this.searchResult.samples= "<table> </table>"
    this.from.sentence = ""
  }
  async reportTranslation(){
    var controller = this;  
    var url = controller.url;
    var reportRoute = controller.searchRoute;
    var translationResult = controller.translationResult;
    const toast = await controller.toastController.create({
        message: 'Translation error reported',
        duration: 1000
    });
    if(controller.translationResult.translated !=""){
      controller.apiService.sendPostRequest(url + reportRoute.get('french','lingala').value, translationResult).then(function(value: any){
        toast.present();      
      });
    }
  }
}